#!usr/bin/env python3

import re
from random import randint
import os
import json

from bs4 import BeautifulSoup
import _thread
import time
import requests
from datetime import date
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram import ChatAction
from functools import wraps

import logging
logging.basicConfig(level=logging.ERROR,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


# TODO: Add typing to the project: https://docs.python.org/3/library/typing.html 

def send_typing_action(func):
    """Sends typing action while processing func command."""

    @wraps(func)
    def command_func(*args, **kwargs):
        bot, update = args
        bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.TYPING)
        return func(bot, update, **kwargs)

    return command_func


def get_website_content(url):

    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:64.0) Gecko/20100101 Firefox/64.0'
    headers = {'User-Agent': user_agent}

    try:
        r = requests.get(url, headers=headers)
        return r

    except ValueError:
        return 'cant get data'

@send_typing_action
def imageflip_image(bot, update, args):
    """ create memes using imageflip api """
    chat_id = update.message.chat_id
    username = os.getenv('IMGFLIP_U')
    password = os.getenv('IMGFLIP_PW')
    url = 'https://api.imgflip.com/caption_image'
    template_id = None

    inputmsg = ''
    for a in args:
        inputmsg += a + ' '

    # TODO: add possibility to generate empty image
    try:
        meme_name, text0, text1 = inputmsg.split('--')
    except ValueError as e:
        message = 'Please format message as /meme name of the meme -- text 1 -- text 2'
        bot.send_message(chat_id=chat_id, text=message)
        return 0

    with open('mems.json', 'r') as meme_file:
        data = json.load(meme_file)

    memes_list = data.get('data').get('memes')
    for meme in memes_list:
        json_meme = meme.get('name').strip().lower()
        if meme_name.strip().lower() == json_meme:
            template_id = str(meme.get('id'))
            break

    if template_id == None:
        message = 'No meme with that name found'
        bot.send_message(chat_id=chat_id, text=message)
        return 0

    content = {
        'template_id': template_id,
        'username': username,
        'password': password,
        'text0': text0.strip(),
        'text1': text1.strip(),
    }

    try:
        r = requests.post(url, data=content).json()
    except ValueError as e:
        print('kikkeli')
        return e

    image_link = r.get('data').get('url')
    bot.send_photo(chat_id=chat_id, photo=image_link)

@send_typing_action
def list_memes(bot, update):
    chat_id = update.message.chat_id
    with open('mems.json', 'r') as meme_file:
        data = json.load(meme_file)

    meme_list = data.get('data').get('memes')
    message = 'List of available meme templates:\n'
    for meme in meme_list:
        message = message + str(meme.get('name')) + '\n'

    bot.send_message(chat_id=chat_id, text=message)


@send_typing_action
def get_weather(bot, update, args):

    """ get the weather data from ilmatieteenlaitos.fi/saa/turku """
    chat_id = update.message.chat_id

    if not args:
        urls = ['https://ilmatieteenlaitos.fi/saa/turku', 'https://ilmatieteenlaitos.fi/saa/helsinki']
        args = ['Turku', 'Helsinki']
    else:
        urls = ['https://ilmatieteenlaitos.fi/saa/' + str(args[0])]

    message = ''
    for a, url in enumerate(urls):
        r = get_website_content(url)
        content = BeautifulSoup(r.text, 'html.parser')

        """ get sunrise and sunset """

        s = content.find('div', attrs={'class': 'celestial-status-text'})

        try:
            sun_up = s.text
        except AttributeError as e:
            message = 'No city found. Bot currently works only with Finnish cities.'
            bot.send_message(chat_id=chat_id, text=message, parse_mode='HTML')
            return 0

        table = content.find('table', attrs={'class': 'meteogram'})

        """ get times and format it for the output message """

        table_head = table.find('thead')
        times = []
        index = (0, 3, 6, 9)
        _times = table_head.text.strip().split()

        i = 0
        for time in _times:
            if time.isdigit():
                if i in index:
                    times.append(time)
                i += 1

        time_message = ''
        for time in times:
            time_message = time_message + str(time) + ':00   '

        """ get weather symbols """

        weather_symbols = table.find('tr', attrs={'class': 'meteogram-weather-symbols'})

        symbols = []
        for i, ws in enumerate(weather_symbols.contents):
            if i % 2 == 1:
                symbols.append(ws.contents[1].attrs['class'][1][5:])

        _symbols = []
        for i in index:
            _symbols.append(symbols[i])

        # emojis to use 🌨 ☁️ 🌧 ☀️ 🌤 🌩 to indicate weather

        symbols_dict = {'snow': ('148', '157', '158', '147', '47', '147', '57', '58', '48'),
                        'cloudy': ('107', '4', '104', '108', '7', '6', '117', '74'),
                        'water': ('24', '37', '137', '138', '38', '111', '34', '124'),
                        'sunny': ('1', '101', '41', '21', '121', '131', '71'),
                        'mixed': ('6', '4', '104', '102', '106', '2')
                        }

        symbols_message = ''
        for symbol in _symbols:
            if symbol in symbols_dict['snow']:
                symbols_message = symbols_message + '🌨' + '      '

            elif symbol in symbols_dict['cloudy']:
                symbols_message = symbols_message + '☁️' + '      '

            elif symbol in symbols_dict['water']:
                symbols_message = symbols_message + '🌧️' + '      '

            elif symbol in symbols_dict['sunny']:
                symbols_message = symbols_message + '☀️' + '      '

            elif symbol in symbols_dict['mixed']:
                symbols_message = symbols_message + '🌤' + '      '

            else:
                symbols_message = symbols_message + symbol + '    '

        """ get temperature and format it for the output message """

        temperatures = table.find('tr', attrs={'class': 'meteogram-temperatures'})
        temperatures = temperatures.text.strip().split()

        _temperatures = []
        for i, ti in enumerate(temperatures):
            if i in index:
                tmp = str(ti) + 'C'
                _temperatures.append(tmp)

        temperature_message = ''
        for temperature in _temperatures:
            if 4 < len(temperature):
                temperature_message = temperature_message + str(temperature) + '   '

            elif len(temperature) == 4:
                temperature_message = temperature_message + str(temperature) + '    '

            elif len(temperature) == 3:
                temperature_message = temperature_message + str(temperature) + '     '

        message = (
                f'{message}'
                f'<code>Weather in {args[a]} is:\n'
                f'{time_message}\n'
                f'{symbols_message}\n'
                f'{temperature_message}\n'
                f'Sun is up: {sun_up}</code>\n\n'
                )

    bot.send_message(chat_id=chat_id, text=message, parse_mode='HTML')


@send_typing_action
def test(bot, update):
    chat_id = update.message.chat_id
    bot.send_message(chat_id=chat_id, text='<code> penis 💩 </code>', parse_mode='HTML')


@send_typing_action
def comic(bot, update):
    """ get a random comic from my favorite artists """

    chat_id = update.message.chat_id
    rnd = randint(0, 2)

    if rnd == 0:
        """ get random comic from xkcd """

        url = 'https://c.xkcd.com/random/comic/'
        r = get_website_content(url)

        body = r.text
        a = [m.start() for m in re.finditer('https://imgs.xkcd.com/comics/', body)]
        i = a[0]

        _link = body[i:i+150]
        newline = _link.find('\n')
        image = body[i:i+newline]

    elif rnd == 1:
        """ get random comic from loadingartist.com """

        url = 'https://loadingartist.com/random'
        r = get_website_content(url)
        body = r.text
        a = [m.start() for m in re.finditer('/wp-content/uploads/', body)]
        i = a[0]

        _link = body[i:i+150]
        end_of_link = _link.find('\"')
        _image = body[i:i+end_of_link]
        image = 'https://loadingartist.com' + _image

    elif rnd == 2:
        """ get random comic from explosm.com """

        url = 'http://explosm.com/comics/random'
        r = get_website_content(url)
        body = r.text
        a = [m.start() for m in re.finditer('//files.explosm.net/comics/', body)]
        i = a[0]

        _link = body[i:i+150]
        end_of_link = _link.find('.png')
        if end_of_link != -1:
            _image = body[i:i+end_of_link+4]
            image = 'http:' + _image
        else:
            end_of_link = _link.find('.gif')
            _image = body[i:i+end_of_link+4]
            image = 'http:' + _image

    elif rnd == 3:
        pass

    else:
        image = 'https://imgs.xkcd.com/comics/error_bars.png'

    if image[-3:] == 'jpg' or image[-3:] == 'png':
        bot.send_photo(chat_id=chat_id, photo=image)

    elif image[-3:] == 'gif':
        bot.send_animation(chat_id=chat_id, animation=image)


@send_typing_action
def lunch_menu(bot, update):
    chat_id = update.message.chat_id

    # roots
    url = 'https://rootskitchen.fi/kasvisruokalounas-hakaniemi-helsinki/#tanaan'

    r = get_website_content(url)

    bs = BeautifulSoup(r.text, 'html.parser')

    try:
        today = bs.find(name='div', attrs={'id': 'tanaan'})
        options_arr = today.find_all('p')

        # don't know if this is a working fix, but well see next week
        if len(options_arr) == 1:
            options = today.find(name='div', attrs={'class': 'yj6qo'}).text
        
        else:
            options = ''
            for option in options_arr:
                options += option.text + '\n'

        roots_lunch = f'Roots: \n{options}\n{url}\n'

    except AttributeError as e:
        logging.error(e)
        roots_lunch = f'Roots: \nNo lunch today\n{url}\n'


    # raiku
    url = 'https://raiku.net/'

    r = get_website_content(url)

    bs = BeautifulSoup(r.text, 'html.parser')

    try:
        div_content = bs.find(name='div', attrs={'class': 'content'})

        if date.today().weekday() == 0:
            options_arr = div_content.find_all('li')[:3]
        elif date.today().weekday() == 1:
            options_arr = div_content.find_all('li')[3:6]
        elif date.today().weekday() == 2:
            options_arr = div_content.find_all('li')[6:9]
        elif date.today().weekday() == 3:
            options_arr = div_content.find_all('li')[9:12]
        elif date.today().weekday() == 4:
            options_arr = div_content.find_all('li')[12:15]
        else:
            raise AttributeError

        options = ''
        for option in options_arr:
            options += option.text + '\n'

        raiku_lunch = f'Raiku:  \n{options}\n{url}\n'

    except AttributeError:
        raiku_lunch = f'Raiku: \nNo lunch today\n'

    # juttutupa
    url = 'https://www.juttutupa.fi/ruokalistat/lounas/'

    r = get_website_content(url)

    bs = BeautifulSoup(r.text, 'html.parser')

    try:
        entry_content = bs.find(name='div', attrs={'class': 'entry-content'})
        week_lunches = entry_content.find_all('p')
        if date.today().weekday() < 5:
            options = week_lunches[date.today().weekday()+2].text

            juttutupa_lunch = f'Juttutupa:  \n{options}\n{url}\n'
        else:
            juttutupa_lunch = f'Juttutupa: \nNo lunch today\n{url}\n'

    except AttributeError:
        juttutupa_lunch = f'Juttutupa: \nNo lunch today\n{url}\n'

    msg = (
        f'{roots_lunch}\n'
        f'{raiku_lunch}\n'
        f'{juttutupa_lunch}'
    )

    bot.send_message(chat_id=chat_id, text=msg)


@send_typing_action
def lunch_menu_old(bot, update):
    """ send 3 day menu for amica as a text"""

    chat_id = update.message.chat_id
    url = 'https://www.amica.fi/modules/json/json/Index?costNumber=0529&language=fi'

    r = get_website_content(url)

    json_obj = r.json()

    lunches = []
    _lunches = ''

    for i in range(len(json_obj['MenusForDays'])):
        _lunches = ''
        if json_obj['MenusForDays'][i]['LunchTime'] != 'suljettu':
            for j in range(len(json_obj['MenusForDays'][i]['SetMenus'])):
                tmp = json_obj['MenusForDays'][i]['SetMenus'][j]['Components']
                if len(tmp) is not 0:
                    _lunches = _lunches + json_obj['MenusForDays'][i]['SetMenus'][j]['Components'][0] + '\n'
        else:
            _lunches = _lunches + "Closed!\n"

        lunches.append(_lunches)

    if len(lunches) >= 7:
        msg = (
              f'Today: \n{lunches[0]}\n'
              f'Tomorrow: \n{lunches[1]}\n'
              f'The day after: \n{lunches[2]}\n'
              f'After that: \n{lunches[3]}\n'
              f'Aaaand after that: \n{lunches[4]}'
            )

    elif len(lunches) == 6:
        msg = (
              f'Today: \n{lunches[0]}\n'
              f'Tomorrow: \n{lunches[1]}\n'
              f'The day after: \n{lunches[2]}\n'
              f'After that: \n{lunches[3]}\n'
            )

    elif len(lunches) == 5:
        msg = (
              f'Today: \n{lunches[0]}\n'
              f'Tomorrow: \n{lunches[1]}\n'
              f'The day after: \n{lunches[2]}\n'
            )

    elif len(lunches) == 4:
        msg = (
              f'Today: \n{lunches[0]}\n'
              f'Tomorrow: \n{lunches[1]}\n'
            )

    elif len(lunches) == 3:
        msg = f'Today: \n{lunches[0]}\n'

    elif len(lunches) < 3:
        msg = 'Closed for rest of the week <3'

    bot.send_message(chat_id=chat_id, text=msg)


@send_typing_action
def commands(bot, update):
    chat_id = update.message.chat_id
    message = 'Current commands: \n' \
              '/w <cityname> - weather for the city \n' \
              '/comic - get a "random" comic\n' \
              '/lunch - lunch for few places in Hakaniemi\n' \
              '/sponge <text> - spongefy the text\n' \
              '/remindme <time in minutes> <reminder text> - set a reminder to X minutes from now or you can use /remindme 15:00 <reminder text>\n' \
              '/meme <name of the meme template>--<text 1>--<text 2>\n' \
              '/memelist gets list of available memes for /meme command'

    bot.send_message(chat_id=chat_id, text=message)


@send_typing_action
def sort_messages(bot, update):
    chat_id = update.message.chat_id
    bot.send_message(chat_id=chat_id, text=update.message.text)


@send_typing_action
def unknown(bot, update):
    chat_id = update.message.chat_id
    message = f'{update.message.text}   <- is not valid command. type /commands to show available commands'
    bot.send_message(chat_id=chat_id, text=message)


@send_typing_action
def sponge_bob(bot, update, args):
    chat_id = update.message.chat_id

    text = ''
    message = ''
    for a in args:
        text += a + ' '

    for i, t in enumerate(text):
        if i % 2:
            message += t.upper()
        else:
            message += t

    bot.send_message(chat_id=chat_id, text=message)


@send_typing_action
def poopipie(bot, update):
    chat_id = update.message.chat_id

    PDPurl = 'https://socialblade.com/youtube/user/pewdiepie/realtime'
    TSurl = 'https://socialblade.com/youtube/user/tseries/realtime'

    PDPr = get_website_content(PDPurl)
    TSr = get_website_content(TSurl)

    bs = BeautifulSoup(PDPr.text, 'html.parser')
    raw_count_p = bs.find('p', attrs={'id': 'rawCount'})
    raw_count_p = str(raw_count_p)
    pdp_count = raw_count_p[40:-4]

    bs = BeautifulSoup(TSr.text, 'html.parser')
    raw_count_p = bs.find('p', attrs={'id': 'rawCount'})
    raw_count_p = str(raw_count_p)
    ts_count = raw_count_p[40:-4]

    msg = (
        f'<code>Pewdiepie is at  {pdp_count}   subscribers\n'
        f'T-Series is at   {ts_count}   subscribers\n'
        f'The cap is       {str(int(pdp_count)-int(ts_count))}</code>'
        )

    bot.send_message(chat_id=chat_id, text=msg, parse_mode='HTML')


@send_typing_action
def bus_time_table(bot, update, args):
    chat_id = update.message.chat_id
    if not args:
        stop = '768'
        url = 'http://data.foli.fi/siri/sm/768/format'
    else:
        stop = str(args[0]).upper()
        url = 'http://data.foli.fi/siri/sm/' + str(args[0]).upper() + '/format'

    stop_name_url = 'http://data.foli.fi/siri/sm/pretty'
    r_stop_name = get_website_content(stop_name_url)

    stop_json = r_stop_name.json()
    stop_name = stop_json[stop]['stop_name']

    r = get_website_content(url)
    bus_json = r.json()

    bus = {'number': [], 'expected': [], 'destination': []}
    for i in range(len(bus_json['result'])):
        bus['expected'].append(bus_json['result'][i]['expectedarrivaltime'].split(' '))
        bus['number'].append(bus_json['result'][i]['lineref'])
        bus['destination'].append(bus_json['result'][i]['destinationdisplay']) 

    msg = f"<code>Next buses for stop {stop} {stop_name} are: \n"

    for i, _ in enumerate(bus_json['result']):
        if i == 10:
            break
        else:
            msg += f"{bus['number'][i]}   {bus['expected'][i][1]} to {bus['destination'][i]}\n"

    if msg == f"<code>Next buses for stop {stop} {stop_name} are: \n":
        msg = 'No more buses for today'
    else:
        msg += "</code>"

    bot.send_message(chat_id=chat_id, text=msg, parse_mode='HTML')


@send_typing_action
def stops(bot, update):
    chat_id = update.message.chat_id
    msg = 'Otavanaukio: 768\nPihkalankatu: 883\nKeskusta Vakkelle: T4\nKeskusta Pansioon: T3'
    
    bot.send_message(chat_id=chat_id, text=msg)


def reminder_delay(bot, chat_id, reminder_message, hours, minutes):
    if not hours:
        time.sleep(int(minutes)*60)
    else:
        current_hour = time.localtime().tm_hour
        current_minute = time.localtime().tm_min
        diff_hour = int(hours) - current_hour
        diff_min = int(minutes) - current_minute
        
        if diff_min < 0:
            diff_min += 60
        
        if diff_hour < 0:
            diff_hour += 24
        
        delay = diff_hour * 60 + diff_min
        time.sleep(delay*60)

    if len(reminder_message) == 0:
        if not hours:
            message = f'Hey its been {minutes} minutes!'
        else:
            message = f'Hey its {hours}:{minutes}!' 

    else:
        if not hours:
            message = f'Hey it\'s been {minutes} minutes. Remember to {reminder_message}!'
        else:
            message = f'Hey its {hours}:{minutes}. Remember to {reminder_message}!' 

    bot.send_message(chat_id=chat_id, text=message)


@send_typing_action
def remind_me(bot, update, args):
    chat_id = update.message.chat_id
    reminder_message = ''
    hours = ''
    print(args)

    if not args:
        message = f'gimme some values'
        bot.send_message(chat_id=chat_id, text=message)
        return 0
    
    if args[0].isdigit():
        minutes = args[0]
    
    elif ':' in args[0]:
        hours, minutes = args[0].split(':')
        
        if not hours.isdigit() and not minutes.isdigit():
            message = f'gimme time in format MM:ss'
            bot.send_message(chat_id=chat_id, text=message)
            return 0

    else:
        message = f'gimme time'
        bot.send_message(chat_id=chat_id, text=message)
        return 0

    if len(args) == 1 and not hours:
        if not hours:
            message = f'I\'ll remind you in {minutes} minutes'
            bot.send_message(chat_id=chat_id, text=message)
        else:
            message = f'I\'ll remind you at {hours}:{minutes}!'
            bot.send_message(chat_id=chat_id, text=message)
    
    else:
        for i, a in enumerate(args):
            if i == 0:
                pass
            else:
                reminder_message = reminder_message + ' ' + a
        
        if not hours:
            message = f'I\'ll remind you in {minutes} minutes to {reminder_message}'
            bot.send_message(chat_id=chat_id, text=message)
        
        elif hours:
            message = f'I\'ll remind you at {hours}:{minutes} to {reminder_message}!'
            bot.send_message(chat_id=chat_id, text=message)

        else:
            # this shouldn't happen
            message = f'ERROR'
            bot.send_message(chat_id=chat_id, text=message)
    
    _thread.start_new_thread(reminder_delay, (bot, chat_id, reminder_message, hours, minutes))


def main():
    telegram_token = os.getenv('T_TOKEN')
    updater = Updater(telegram_token)
    dp = updater.dispatcher

    dp.add_handler(CommandHandler('w', get_weather, pass_args=True))
    dp.add_handler(CommandHandler('test', test))
    dp.add_handler(CommandHandler('comic', comic))
    dp.add_handler(CommandHandler('lunch_old', lunch_menu_old))
    dp.add_handler(CommandHandler('lunch', lunch_menu))
    dp.add_handler(CommandHandler('pdp', poopipie))
    dp.add_handler(CommandHandler('commands', commands))
    dp.add_handler(CommandHandler('sponge', sponge_bob, pass_args=True))
    dp.add_handler(CommandHandler('bussi', bus_time_table, pass_args=True))
    dp.add_handler(CommandHandler('remindme', remind_me, pass_args=True))
    dp.add_handler(CommandHandler('pysäkit', stops))
    dp.add_handler(CommandHandler('meme', imageflip_image, pass_args=True))
    dp.add_handler(CommandHandler('memelist', list_memes))

    dp.add_handler(MessageHandler(Filters.text, callback=sort_messages))
    dp.add_handler(MessageHandler(Filters.command, callback=unknown))

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
